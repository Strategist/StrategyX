/*
 * Renderer.js ~ Contains an abstract Renderer class to render
 */

function Renderer()
{
    this.self = this
    self.window = null
}

Renderer.prototype =
{
    // Constructor
    constructor: Renderer,

    // Methods
    attachWindow: function(window)
    {
        self.window = window
    },

    drawImage: function (image, x, y, width, height)
    {
        self.window.canvas.getContext("2d").drawImage(image, x, y, width, height)
    },

    drawGUI: function(GUI)
    {

    },

    drawScene: function(GUI)
    {

    }
}

