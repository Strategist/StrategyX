/*
 * Window.js ~ Contains the Window class which represents a window inside the application.
 */

function Window(id)
{
    this.self = this
    self.canvas = document.getElementById(id)
}

Window.prototype = 
{
	// Constructor
	constructor: Window,
	
    // Methods
	setFullscreen: function()
	{
	    self.canvas.width = window.innerWidth
	    self.canvas.height = window.innerHeight
	},
	
	// Getters and Setters
	setHeight: function(height)
	{
	    self.canvas.height = height
	},

	getHeight: function()
	{
	    return self.canvas.height
	},

    setWidth: function(width)
    {
        self.canvas.width = width
    },

    getWidth: function()
    {
        return self.canvas.width
    }
}

